---
title: Static Site Generators
comments: true
---

## This site is built with a Static Site Generator

This site is currently leveraging the [Hugo framework](https://gohugo.io/).  Hugo is a Static Site Generator (SSG)

Since Madmongers is a _Perl_ user group, we will assess the landscape of SSGs built with Perl.  This site will remain using Hugo until the optimal Perl-based SSG is identified (or built)

Check out the following blog posts on this topic
* [Static Site Generators]({{< ref "/post/2020-01-12-ssg.md" >}})
