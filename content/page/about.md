---
title: About Madmongers
subtitle: Who are the MadMongers
comments: false
---

MadMongers is a Perl Mongers user group in Madison, Wisconsin:

- Join us at our [monthly meetups](https://meetup.com/madmongers)
- Check out other stuff on this web site

### MadMongers history

We are a long-established community of Perl programmers.  We welcome newcomers from outside of the Perl community.
